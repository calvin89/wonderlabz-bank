
## Name
Wonder Bank Application

## Description
A simple API for a test  banking platform. Operations Include, but not limited to:

Create Customer Bank Account

Deposit Funds into a Customer Account

Withdraw Funds from a Customer Bank Account

## API Endpoint Documentation
Documentation for this app can be accessed after running this app on: http://localhost:8080/swagger-ui.html# 

## Installation

Building package : mvn clean install

Run application : mvn spring-bootrun

## Roadmap
 
Add an Enterprise Database
	
Implement Oauth2 auhtentication? Security
	
Build Microservices according to business domain
	
Containerise using Docker
	
Manage Containers using Kubernetes
	
	
#To Add the following modules in form of Microservices:
	
User Accounts / Authorization Server

Treasury

KYC

Loans
	
General Ledger
	
Term Deposits
	
Day end/ Month end/ Year end Jobs




